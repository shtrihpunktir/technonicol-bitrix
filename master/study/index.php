<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("");
?>
    <section class="section section-learning-promo">
        <div class="section-cell">
            <div class="learning-promo">
                <h1>Пройди обучение в Строительной Академии ТЕХНОНИКОЛЬ у профессионалов</h1>
                <p>Самостоятельно строите дом, дачу, делаете ремонт в квартире, но не хватает знаний по выбору и применению материалов?<br>
                    Наши мастера готовы ответить на ваши вопросы, поделиться своими знаниями и опытом!</p>
                <div class="learning-types">
                    <div class="learning-type">
                        <div class="inner">
                            <h2>Очное обучение </h2>
                            <div class="descr">
                                <p><strong>Не упустите уникальную возможность:</strong></p>
                            </div>
                            <div class="advantages">
                                <div class="item">побывать на заводах и увидеть производство материалов своими глазами;</div>
                                <div class="item">лично пообщаться со специалистами, задать интересующие вас вопросы;</div>
                                <div class="item">получить практические навыки работы с материалами, используемыми для фундаментов, дорожек, кровель, стен и фасадов, балконов и бань.</div>
                            </div>
                            <p><strong>РЕГИСТРИРУЙТЕСЬ НА НАШИ КУРСЫ, А ВСЕ ОСТАЛЬНОЕ МЫ БЕРЕМ НА СЕБЯ!</strong></p>
                            <div class="more mobile-only">
                                <a href="./offline" class="btn">Посмотреть очные курсы</a>
                            </div>
                        </div>
                    </div>
                    <div class="learning-type">
                        <div class="inner">
                            <h2>Онлайн обучение </h2>
                            <div class="descr">
                                <p><strong>Вы узнаете максимум информации за минимальное время, разберетесь в видах материалов, их применении, плюсах и минусах конкретного выбора:</strong></p>
                            </div>
                            <div class="advantages">
                                <div class="item">учитесь из своего города, не выходя из дома, на работе, в отпуске;</div>
                                <div class="item">формируйте программу и темы занятий под себя;</div>
                                <div class="item">узнайте, как подобрать подходящие материалы для своего объекта.</div>
                            </div>
                            <p><strong>ВЫБИРАЙТЕ ВЕБИНАР, РЕГИСТРИРУЙТЕСЬ И ДО ВСТРЕЧИ В ЭФИРЕ!</strong></p>
                            <div class="more mobile-only">
                                <a href="./online" class="btn">Посмотреть онлайн-курсы</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="free">
                    <p><strong>И ГЛАВНОЕ, ЭТО АБСОЛЮТНО БЕСПЛАТНО!!!</strong></p>
                </div>
                <div class="learning-types">
                    <div class="learning-type">
                        <div class="more desktop-only">
                            <a href="./offline" class="btn">Посмотреть очные курсы</a>
                        </div>
                    </div>
                    <div class="learning-type">
                        <div class="more desktop-only">
                            <a href="./online" class="btn">Посмотреть онлайн-курсы</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dual-bg">
            <div class="item"
                 style="background-image: url(<?= SITE_TEMPLATE_PATH ?>/images/main_block_course_02_01.jpg)"></div>
            <div class="item"
                 style="background-image: url(<?= SITE_TEMPLATE_PATH ?>/images/main_block_course_02_02.jpg)"></div>
        </div>
    </section>

<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
?>