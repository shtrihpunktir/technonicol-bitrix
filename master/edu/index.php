<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$asset = Bitrix\Main\Page\Asset::getInstance();
$asset->addJs(SITE_TEMPLATE_PATH . '/vendor/jquery/dist/jquery.min.js');
$asset->addJs(SITE_TEMPLATE_PATH . '/vendor/slick-carousel/slick/slick.min.js', true);
$asset->addCss(SITE_TEMPLATE_PATH . '/vendor/slick-carousel/slick/slick.css', true);
$asset->addCss(SITE_TEMPLATE_PATH . '/vendor/slick-carousel/slick/slick-theme.css', true);
$asset->addJs(SITE_TEMPLATE_PATH . '/js/learning.js', true);
?>
    <section class="section section-learning intramural">
        <div class="section-cell">
            <div class="learning">
                <h1>Очные курсы и экскурсии <span>с&nbsp;ТЕХНОНИКОЛЬ</span></h1>
                <div class="descr">
                    Мы разработали авторские очные курсы для людей, которые планируют самостоятельный ремонт квартиры
                    или строительство дома, дачи. У вас будет уникальная возможность не только получить необходимые
                    знания, но и увидеть, как материалы производятся, попробовать их в работе под руководством мастера!
                </div>

                <? $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "learning_offline_timeline",
                    array(
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "ADD_SECTIONS_CHAIN" => "Y",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "N",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "FIELD_CODE" => array(
                            0 => "",
                            1 => "",
                        ),
                        "FILTER_NAME" => "",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "IBLOCK_ID" => \TAO::infoblock('learning_offline')->id(),
                        "IBLOCK_TYPE" => "learning",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "MESSAGE_404" => "",
                        "NEWS_COUNT" => "20",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Новости",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "PROPERTY_CODE" => array(
                            1 => "DATE_START",
                            2 => "DATE_END",
                        ),
                        "SET_BROWSER_TITLE" => "Y",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_META_DESCRIPTION" => "Y",
                        "SET_META_KEYWORDS" => "Y",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "Y",
                        "SHOW_404" => "N",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER1" => "DESC",
                        "SORT_ORDER2" => "ASC",
                        "STRICT_SECTION_CHECK" => "N"
                    ),
                    false
                ); ?>
                <div class="rules-link">
                    <a href="/tn-rules-offline.pdf" target="_blank" rel="noopener" class="btn btn-light btn-white">Правила
                        прохождения курсов</a>
                </div>
            </div>
        </div>
    </section>


    <section class="section section-gallery">
        <div class="section-gallery-heading"><h2 style="max-width: none">Очные курсы и экскурсии<br> с&nbsp;ТЕХНОНИКОЛЬ
            </h2></div>
        <div class="gallery">
            <?php
            $slides = [
                ['name' => 'Строительная академия ТЕХНОНИКОЛЬ', 'url' => SITE_TEMPLATE_PATH . "/images/slider/offline_0.jpg"],
                ['name' => 'Строительная академия ТЕХНОНИКОЛЬ', 'url' => SITE_TEMPLATE_PATH . "/images/slider/offline_10.jpg"],
                ['name' => 'Строительная академия ТЕХНОНИКОЛЬ', 'url' => SITE_TEMPLATE_PATH . "/images/slider/offline_20.jpg"],
                ['name' => 'Завод по производству теплоизоляционных плит PIR', 'url' => SITE_TEMPLATE_PATH . "/images/slider/offline_30.jpg"],
                ['name' => 'Завод по производству теплоизоляционных плит PIR', 'url' => SITE_TEMPLATE_PATH . "/images/slider/offline_40.jpg"],
                ['name' => 'Завод Техно по производству теплоизоляционных материалов', 'url' => SITE_TEMPLATE_PATH . "/images/slider/offline_50.jpg"],
                ['name' => 'Завод Техно по производству теплоизоляционных материалов', 'url' => SITE_TEMPLATE_PATH . "/images/slider/offline_75.jpg"],
                ['name' => 'Завод по производству гибкой черепицы ТЕХНОНИКОЛЬ SHINGLAS', 'url' => SITE_TEMPLATE_PATH . "/images/slider/offline_70.jpg"],
                ['name' => 'Завод по производству гибкой черепицы ТЕХНОНИКОЛЬ SHINGLAS', 'url' => SITE_TEMPLATE_PATH . "/images/slider/offline_55.jpg"],
            ];
            ?>
            <?php foreach ($slides as $slide) : ?>
                <div class="slide">
                    <h4><?= $slide['name'] ?></h4>
                    <div class="image" style="background-image: url(<?= $slide['url'] ?>)"></div>
                </div>
            <?php endforeach; ?>
        </div>
    </section>

    <section class="section section-courses">
        <h2 class="section-heading">Ближайшие курсы</h2>
        <? $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "learning_offline",
            array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "ADD_SECTIONS_CHAIN" => "Y",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "N",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "FILTER_NAME" => "",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => \TAO::infoblock('learning_offline')->id(),
                "IBLOCK_TYPE" => "learning",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                "INCLUDE_SUBSECTIONS" => "Y",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "20",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "Новости",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PROPERTY_CODE" => array(
                    1 => "DATE_START",
                    2 => "DATE_END",
                    3 => "REVIEW_FORM_URL",
                ),
                "SET_BROWSER_TITLE" => "Y",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "Y",
                "SET_META_KEYWORDS" => "Y",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "Y",
                "SHOW_404" => "N",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_BY2" => "SORT",
                "SORT_ORDER1" => "DESC",
                "SORT_ORDER2" => "ASC",
                "STRICT_SECTION_CHECK" => "N"
            ),
            false
        ); ?>
    </section>

<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
?>