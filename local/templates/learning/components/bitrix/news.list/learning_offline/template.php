<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */


CUtil::InitJSCore(array('ajax', 'popup'));
$this->setFrameMode(true);
$REGISTRATION_FORM = '__registration_form'
?>

<script>
    var formPopup, successPopup;

    function showRegistrationPopup(id, name) {
        var modal = BX('<?=$REGISTRATION_FORM?>');
        if (!formPopup) {
            formPopup = new BX.PopupWindow('<?=$REGISTRATION_FORM?>__bx_popup', window.body, {
                autoHide: true,
                offsetTop: 1,
                offsetLeft: 0,
                lightShadow: true,
                closeIcon: false,
                closeByEsc: true,
                overlay: {
                    opacity: '80'
                }
            });
            formPopup.setContent(modal);
        }
        if (!successPopup) {
            successPopup = new BX.PopupWindow('<?=$REGISTRATION_FORM?>_success__bx_popup', window.body, {
                autoHide: true,
                offsetTop: 1,
                offsetLeft: 0,
                lightShadow: true,
                closeIcon: false,
                closeByEsc: true,
                overlay: {
                    opacity: '80'
                }
            });
            successPopup.setContent(BX('registration_done'));
        }
        window.COURSE_REGISTRATION_SENDING = false;
        var courseTitle = modal.getElementsByClassName('course-title').item(0);
        courseTitle.innerHtml = "&laquo;" + name + "&raquo;";
        var form = modal.getElementsByTagName('form').item(0);
        form.reset();
        form.course.value = id;
        form.onsubmit = function submit(event) {
            event.preventDefault();
            if (window.COURSE_REGISTRATION_SENDING) return;
            var form = event.target;
            var data = ['name', 'phone', 'email', 'course'].reduce(function (acc, field) {
                acc[field] = form[field].value;
                return acc;
            }, {});
            window.COURSE_REGISTRATION_SENDING = true;
            BX.ajax({
                url: '/master/ajax/learning/offline/register',
                method: 'POST',
                data: data,
                async: true,
                start: true,
                onsuccess: function (data) {
                    formPopup.close();
                    successPopup.show();
                    window.COURSE_REGISTRATION_SENDING = false;
                },
                onfailure: function (data) {
                    console.log('error', ...arguments);
                    window.COURSE_REGISTRATION_SENDING = false;
                },
            })
        };

        formPopup.show();
    }

</script>
<div class="courses-list">
    <div class="courses-list-container">
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <?php
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

            $dateStart = MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]['DATE_START']['VALUE']);
            $dateEnd = MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]['DATE_END']['VALUE']);
            $isFinished = time() > $dateEnd;
            $isSameMonth = date('n', $dateStart) === date('n', $dateEnd);
            ?>
            <article class="course-box" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <div class="course-box-inner" id="item_<?=$arItem['ID']?>">
                    <div class="sidebar">
                        <div class="thumb">
                            <img
                                    border="0"
                                    src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                    alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                                    title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>"
                            />
                        </div>
                        <div class="actions">
                            <?php if ($isFinished) : ?>
                                <?php if ($arItem["DISPLAY_PROPERTIES"]['REVIEW_FORM_URL']['VALUE']) : ?>
                                    <a href="<?= $arItem["DISPLAY_PROPERTIES"]['REVIEW_FORM_URL']['VALUE'] ?>"
                                       rel="noopener"
                                       target="_blank"
                                       class="btn">
                                        Оставить отзыв
                                    </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <button class="btn"
                                        onclick="showRegistrationPopup('<?= $arItem['ID'] ?>', '<?= $arItem['NAME'] ?>')">
                                    Зарегистрироваться
                                </button>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="main-col">

                        <div class="course-header">

                            <h3><?= $arItem["NAME"] ?></h3>
                            <div class="course-info">
                                <div class="item">
                                    <span class="icon">
                                        <i class="far fa-calendar-alt"></i>
                                    </span>
                                    <?php if ($dateStart === $dateEnd) : ?>
                                        <span class="date"><?= FormatDate("j F", $dateStart); ?></span>
                                    <?php else : ?>
                                        с
                                        <span class="date"><?= FormatDate($isSameMonth ? "j" : "j F", $dateStart); ?></span>
                                        по <span class="date"><?= FormatDate("j F", $dateEnd); ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <? if ($isFinished) : ?>
                                <div class="flag done">
                                    Завершен
                                </div>
                            <? endif; ?>

                        </div>
                        <div class="course-content">
                            <div class="course-content-brief">
                                <div class="course-descr"><?= $arItem['PREVIEW_TEXT'] ?></div>
                            </div>
                            <div class="course-content-full">
                                <div class="course-desc"><?= $arItem['DETAIL_TEXT'] ?></div>

                                <?
                                $filterName = 'filter_reviews_' . $arItem['ID'];
                                $GLOBALS[$filterName] = [
                                    'PROPERTY_REFERRED_TO' => $arItem['ID']
                                ];
                                $APPLICATION->IncludeComponent(
                                    "bitrix:news.list",
                                    "review_slider",
                                    array(
                                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                        "ADD_SECTIONS_CHAIN" => "N",
                                        "AJAX_MODE" => "N",
                                        "AJAX_OPTION_ADDITIONAL" => "",
                                        "AJAX_OPTION_HISTORY" => "N",
                                        "AJAX_OPTION_JUMP" => "N",
                                        "AJAX_OPTION_STYLE" => "N",
                                        "CACHE_FILTER" => "N",
                                        "CACHE_GROUPS" => "Y",
                                        "CACHE_TIME" => "36000000",
                                        "CACHE_TYPE" => "A",
                                        "CHECK_DATES" => "Y",
                                        "DETAIL_URL" => "",
                                        "DISPLAY_BOTTOM_PAGER" => "Y",
                                        "DISPLAY_DATE" => "Y",
                                        "DISPLAY_NAME" => "Y",
                                        "DISPLAY_PICTURE" => "Y",
                                        "DISPLAY_PREVIEW_TEXT" => "Y",
                                        "DISPLAY_TOP_PAGER" => "N",
                                        "FIELD_CODE" => array(
                                            0 => "",
                                            1 => "",
                                        ),
                                        "FILTER_NAME" => $filterName,
                                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                        "IBLOCK_ID" => \Tao::infoblock('learning_offline_review')->getId(),
                                        "IBLOCK_TYPE" => "learning",
                                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                                        "INCLUDE_SUBSECTIONS" => "Y",
                                        "MESSAGE_404" => "",
                                        "NEWS_COUNT" => "20",
                                        "PAGER_BASE_LINK_ENABLE" => "N",
                                        "PAGER_DESC_NUMBERING" => "N",
                                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                        "PAGER_SHOW_ALL" => "N",
                                        "PAGER_SHOW_ALWAYS" => "N",
                                        "PAGER_TEMPLATE" => ".default",
                                        "PAGER_TITLE" => "Новости",
                                        "PARENT_SECTION" => "",
                                        "PARENT_SECTION_CODE" => "",
                                        "PREVIEW_TRUNCATE_LEN" => "",
                                        "SET_BROWSER_TITLE" => "Y",
                                        "SET_LAST_MODIFIED" => "N",
                                        "SET_META_DESCRIPTION" => "N",
                                        "SET_META_KEYWORDS" => "N",
                                        "SET_STATUS_404" => "N",
                                        "SET_TITLE" => "N",
                                        "SHOW_404" => "N",
                                        "SORT_BY1" => "ACTIVE_FROM",
                                        "SORT_BY2" => "SORT",
                                        "SORT_ORDER1" => "DESC",
                                        "SORT_ORDER2" => "ASC",
                                        "STRICT_SECTION_CHECK" => "N",
                                        "COMPONENT_TEMPLATE" => "review_slider"
                                    ),
                                    $component,
                                    [
                                        'HIDE_ICONS' => 'Y'
                                    ]
                                );
                                ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="expand-toggle">
                    <div class="show">
                        <i class="fa fa-chevron-down"></i>
                        <span>Подробнее</span>
                    </div>
                    <div class="hide">
                        <i class="fa fa-chevron-up"></i>
                        <span>Скрыть</span>
                    </div>
                </div>

            </article>
        <? endforeach; ?>
    </div>
</div>

<div class="modal-box" style="display: none" id="<?= $REGISTRATION_FORM ?>">
    <h3 class="modal-title">
        Запись на курс
        <strong class="course-title">“Фасад + внутренние работы”</strong>
    </h3>
    <span class="close" onclick="formPopup.close()">×</span>
    <div class="modal-form">
        <form>
            <div class="form-row">
                <div class="form-control">
                    <input name="name" type="text" required placeholder="Имя и Фамилия">
                </div>
            </div>
            <div class="form-row">
                <div class="form-control">
                    <input name="phone" type="text" required placeholder="Телефон">
                </div>
            </div>
            <div class="form-row">
                <div class="form-control">
                    <input name="email" type="text" required placeholder="E-mail">
                </div>
            </div>
            <div class="form-row">
                <div class="form-control">
                    <div class="checkbox">
                        <label>
                            <span class="check"><input required type="checkbox"/><i></i></span>
                            <span class="checkbox-title">Согласие на обработку персональных данных</span>
                        </label>

                    </div>
                </div>
            </div>
            <div>
                <small>* Дата проведения Курса может быть изменена по техническим причинам, о чем Вы будете проинформированы заблаговременно</small>
            </div>
            <input name="course" type="hidden">
            <div class="submit">
                <button type="submit" class="btn">Записаться</button>
            </div>
        </form>
    </div>
</div>

<div class="modal-box" style="display: none" id="registration_done">
    <h3 class="modal-title">
        Спасибо<br>
        за регистрацию!
    </h3>
    <span class="close" onclick="successPopup.close()">×</span>
    <div class="submit">
        <button class="btn" onclick="successPopup.close()">Закрыть</button>
    </div>
</div>


