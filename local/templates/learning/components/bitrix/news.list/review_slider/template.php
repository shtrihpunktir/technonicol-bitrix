<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$asset = Bitrix\Main\Page\Asset::getInstance();
$asset->addJs(SITE_TEMPLATE_PATH.'/vendor/jquery/dist/jquery.min.js');
$asset->addJs(SITE_TEMPLATE_PATH.'/vendor/slick-carousel/slick/slick.min.js', true);
$asset->addCss(SITE_TEMPLATE_PATH.'/vendor/slick-carousel/slick/slick.css', true);
$asset->addCss(SITE_TEMPLATE_PATH.'/vendor/slick-carousel/slick/slick-theme.css', true);

?>
<? if (!empty($arResult['ITEMS'])): ?>
    <div class="reviews-block">
        <div class="reviews-header">
            <h4>Отзывы (<?= count($arResult['ITEMS']) ?>)</h4>
            <div class="reviews-nav"></div>
        </div>

        <div class="reviews-slider">
            <?php foreach ($arResult['ITEMS'] as $arItem): ?>
                <?php
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

                ?>
                <div class="reviews-item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                    <span class="quote"><i class="fa fa-quote-left"></i></span>
                    <p><strong><?= $arItem['NAME'] ?></strong></p>
                    <?= $arItem['PREVIEW_TEXT'] ?>
                </div>
            <?php endforeach; ?>
        </div>

    </div>
<?php endif; ?>
