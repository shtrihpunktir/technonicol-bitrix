<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

?>
<script>
    function scrollToItem(id) {
        var header = document.getElementsByTagName('header').item(0);
        var item =$('#item_'+id);
        window.scrollTo({
            left:0,
            top: item.offset().top - header.clientHeight,
            behavior: 'smooth'
        })
    }
</script>

<div class="timeline">
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <?php
        $dateStart = MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]['DATE_START']['VALUE']);
        $dateEnd = MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]['DATE_END']['VALUE']);
        $isFinished = time() > $dateEnd;

        ?>

        <div class="step <?= $isFinished ? 'done' : '' ?>">
            <div class="graphics">
                <div class="button"
                     onclick="scrollToItem('<?=$arItem['ID']?>')">
                    <i class="fa <?= $isFinished ? 'fa-check' : 'fa-pencil-ruler' ?>"></i>
                    <div class="timeline-tooltip">
                        <div class="title"><?= $arItem['NAME'] ?></div>
                        <? if ($isFinished): ?>
                            <div class="status">Завершено</div>
                        <? endif; ?>
                    </div>
                </div>
            </div>
            <div class="date">
                <span class="item"><?= FormatDate("j F", $dateStart); ?></span>
                <?php if ($dateStart !== $dateEnd): ?>
                    &mdash;
                    <span class="item"><?= FormatDate("j F", $dateEnd); ?></span>
                <?php endif; ?>
            </div>
        </div>
    <? endforeach; ?>
</div>

