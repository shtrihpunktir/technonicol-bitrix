<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
IncludeTemplateLangFile(__FILE__);
include($_SERVER["DOCUMENT_ROOT"] . "/local/include/redirs.php");
global $APPLICATION;
$asset = Bitrix\Main\Page\Asset::getInstance();

$asset->addJs(SITE_TEMPLATE_PATH.'/vendor/jquery/dist/jquery.min.js');
$asset->addCss(SITE_TEMPLATE_PATH . '/css/reset.css');

$asset->addCss(SITE_TEMPLATE_PATH . '/vendor/font-awesome/css/fontawesome.css');
$asset->addCss(SITE_TEMPLATE_PATH . '/vendor/font-awesome/css/regular.css');
$asset->addCss(SITE_TEMPLATE_PATH . '/vendor/font-awesome/css/solid.css');
$asset->addCss(SITE_TEMPLATE_PATH . '/vendor/font-awesome/css/brands.css');


//foreach (['jquery-3.3.1.min.js', 'svg4everybody.min.js', 'jquery.fancybox.min.js', 'slick.js', 'script.js'] as $src)
////    $asset->addJs(SITE_TEMPLATE_PATH . '/js/' . $src);

?>
<!doctype html>
<html lang="<?= LANGUAGE_ID ?>">
<head>
    <?php $APPLICATION->ShowHead(); ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Теплоизоляция ТЕХНОНИКОЛЬ MASTER для дома и квартиры</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
</head>
<body>
<? $APPLICATION->ShowPanel(); ?>
<header class="page-header">
    <span class="logo"></span>
    <div class="win-action">
        <a href="https://shop.tn.ru/anniversary" target="_blank">
            <span class="desktop">Акция! Выиграйте поездку на Мальдивы!</span>
            <span class="mobile">Акция!<br> Мальдивы!</span>
        </a>
    </div>
    <nav class="main-nav">
        <ul>
            <li><a class="internal-link" href="https://www.tn.ru/master/#main">О Корпорации</a></li>
            <li><a class="internal-link" href="https://www.tn.ru/master/#info">ТЕХНОНИКОЛЬ MASTER</a></li>
        </ul>
    </nav>
    <span class="nav-toggle"><i></i></span>
</header>
