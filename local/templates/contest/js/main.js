$(function(){

    $(window).on("scroll", function() {
        var fromTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
        $('html').toggleClass("scrolled", (fromTop > 1));
    });
    $(window).scroll();

    $('.nav-toggle').on('click', function(){
        $('html').toggleClass('nav-on');
    });

});