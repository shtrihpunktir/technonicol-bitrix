$(function(){

    $('.gallery').slick({
        slidesToShow:1,
        slidesToScroll:1,
        speed:750,
        dots:true,
        prevArrow: '<span class="gallery-prev"><i class="fa fa-chevron-left"></i></span>',
        nextArrow: '<span class="gallery-next"><i class="fa fa-chevron-right"></i></span>'
    });
    $('.reviews-slider').each(function(){
        var navBox = $(this).parent().find('.reviews-header .reviews-nav');
        $(this).slick({
            slidesToShow:1,
            slidesToScroll:1,
            speed:650,
            /*fade:true,*/
            /*dots:true,*/
            appendArrows:navBox,
            prevArrow:'<button class=""><i class="fa fa-chevron-left"></i></button>',
            nextArrow:'<button class=""><i class="fa fa-chevron-right"></i></button>'
        });
    });

    $('.course-box .expand-toggle').click(function(){
        var _courseContentBox = $(this).parent().find('.course-content');
        if (_courseContentBox.hasClass('expand')) {

        } else {

        }
        _courseContentBox.toggleClass('expand');
        $(this).toggleClass('active');
    });

});