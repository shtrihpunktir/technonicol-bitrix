<?php

$arAuthServices = [];
foreach ($arParams["~AUTH_SERVICES"] as &$service) {
    switch (strtolower($service['ID'])) {
        case 'facebook':
            $service['ICON'] = 'fab fa-facebook-f';
            break;
        case 'vkontakte':
            $service['ICON'] = 'fab fa-vk';
            break;
    }
}
