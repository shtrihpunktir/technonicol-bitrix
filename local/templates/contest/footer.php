<section class="section section-share">
    <h3>Расскажи друзьям!</h3>
    <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki"></div>
</section>
<footer class="page-footer">
    <div class="copyright"> &copy; 2018 ООО "ТехноНИКОЛЬ - Строительные системы"</div>
    <div class="nav">
        <ul>
            <li><a href="#" target="_blank">Правила</a></li>
            <li><a href="https://www.tn.ru/" target="_blank">Сайт ТЕХНОНИКОЛЬ</a></li>
            <li><a href="https://shop.tn.ru/" target="_blank">Интернет-магазин</a></li>
        </ul>
    </div>
    <div class="socials">
        <ul>
            <li><a href="https://ok.ru/technonicol" target="_blank"><i class="fab fa-odnoklassniki"></i></a></li>
            <li><a href="https://vk.com/technonicol" target="_blank"><i class="fab fa-vk"></i></a></li>
            <li><a href="https://www.facebook.com/TechnoNICOL/" target="_blank"><i class="fab fa-facebook-f"></i></a>
            </li>
        </ul>
    </div>
</footer>
<?php
$asset = Bitrix\Main\Page\Asset::getInstance();
$asset->addJs(SITE_TEMPLATE_PATH . '/js/main.js', true);
$asset->addJs('https://yastatic.net/share2/share.js', true);
$asset->addCss(SITE_TEMPLATE_PATH . '/css/style.css', true);
?>
</body>
</html>