<?php define('INCLUDE_HEADER', true) ?>
<?php
$asset = Bitrix\Main\Page\Asset::getInstance();

$asset->addJs(SITE_TEMPLATE_PATH . '/vendor/jquery/dist/jquery.min.js');
$asset->addCss(SITE_TEMPLATE_PATH . '/css/reset.css');

$asset->addCss(SITE_TEMPLATE_PATH . '/vendor/font-awesome/css/fontawesome.css');
$asset->addCss(SITE_TEMPLATE_PATH . '/vendor/font-awesome/css/regular.css');
$asset->addCss(SITE_TEMPLATE_PATH . '/vendor/font-awesome/css/solid.css');
$asset->addCss(SITE_TEMPLATE_PATH . '/vendor/font-awesome/css/brands.css');
$PROTOCOL = (CMain::IsHTTPS()) ? "https://" : "http://";
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <title>Покажи, какой ты мастер</title>
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="<?= $PROTOCOL . SITE_SERVER_NAME ?>/master/contest/">
    <meta name="twitter:image" content="<?= $PROTOCOL . SITE_SERVER_NAME . SITE_TEMPLATE_PATH ?>/images/og800.jpg">
    <meta property="og:url" content="<?= $PROTOCOL . SITE_SERVER_NAME ?>/master/contest/">
    <meta property="og:title" content="ПОКАЖИ, КАКОЙ ТЫ МАСТЕР">
    <meta property="og:type" content="website">
    <meta property="og:description"
          content="Участвуй в конкурсе, выиграй профессиональную съемку с твоим участием и подарок, достойный профессионала! А дальше мы расскажем о тебе на просторах интернета.">
    <meta property="og:image" content="<?= $PROTOCOL . SITE_SERVER_NAME . SITE_TEMPLATE_PATH ?>/images/og968.jpg">
    <meta property="og:image:width" content="968">
    <meta property="og:image:height" content="504">
    <meta property="og:image" content="<?= $PROTOCOL . SITE_SERVER_NAME . SITE_TEMPLATE_PATH ?>/images/og1200.jpg">
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    <meta property="og:image" content="<?= $PROTOCOL . SITE_SERVER_NAME . SITE_TEMPLATE_PATH ?>/images/og600.jpg">
    <meta property="og:image:width" content="600">
    <meta property="og:image:height" content="315">
    <meta property="og:image" content="<?= $PROTOCOL . SITE_SERVER_NAME . SITE_TEMPLATE_PATH ?>/images/og640x320.jpg">
    <meta property="og:image:width" content="640">
    <meta property="og:image:height" content="320">
    <meta property="og:image" content="<?= $PROTOCOL . SITE_SERVER_NAME . SITE_TEMPLATE_PATH ?>/images/og800.jpg">
    <meta property="og:image:width" content="800">
    <meta property="og:image:height" content="800">
    <meta property="og:image:alt"
          content="Участвуй в конкурсе, выиграй профессиональную съемку с твоим участием и подарок, достойный профессионала! А дальше мы расскажем о тебе на просторах интернета.">
    <?php $APPLICATION->ShowHead(); ?>
    <style>
        .ya-share2__link {
            transform: scale(2);
            margin: 0 1.5rem;
        }

        .ya-share2__icon {
            background-color: #e30613 !important;
        }

        .ya-share2__badge {
            border-radius: 0 !important;
        }

        @media (max-width: 980px) {
            .ya-share2__link {
                margin: 0 2.5rem;
            }
        }
    </style>
</head>
<body>
<?php $APPLICATION->ShowPanel() ?>
<header class="page-header">
    <span class="logo"></span>
    <div class="win-action">
        <a href="https://shop.tn.ru/anniversary" target="_blank">
            <span class="desktop">Акция! Выиграйте поездку на Мальдивы!</span>
            <span class="mobile">Акция!<br> Мальдивы!</span>
        </a>
    </div>
    <nav class="main-nav">
        <ul>
            <li><a class="internal-link" href="https://www.tn.ru/master/#main">О Корпорации</a></li>
            <li><a class="internal-link" href="https://www.tn.ru/master/#info">ТЕХНОНИКОЛЬ MASTER</a></li>
        </ul>
    </nav>
    <span class="nav-toggle"><i></i></span>
</header>
<section class="section section-promo full-height">
    <div class="promo-block"><h1>Покажи,<br> какой ты мастер</h1>
        <div class="descr"><p>Делаешь классные вещи и мечтаешь, чтобы о твоем мастерстве узнала вся страна? Но пока не
                знаешь, как исполнить мечту?<br> ТЕХНОНИКОЛЬ поможет тебе в этом! Участвуй в конкурсе, выиграй
                профессиональную съемку с твоим участием и подарок, достойный профессионала! А дальше мы расскажем о
                тебе на
                просторах интернета.</p>
            <p>Не жди! Начни двигаться к своей мечте прямо сейчас!</p></div>
        <div class="action">
            <a class="btn btn-large" href="#section-contest">Принять участие</a>
        </div>
    </div>
    <div class="scroll-btn"><i></i></div>
</section>
<section class="section section-prize full-height m-auto-height">
    <div class="section-prize-title"><h2>ежемесячные Призы</h2></div>
    <div>
        <div class="prizes">
            <div class="item">
                <h3>Съёмка профессионального
                    <nobr>DIY-видео</nobr>
                    с твоим участием
                </h3>
                <div class="notice"></div>
                <div class="bg"
                     style="background-image: url(<?= SITE_TEMPLATE_PATH ?>/images/prize-shooting-bg.jpg)"></div>
            </div>
            <div class="item">
                <h3>Набор профессионального инструмента*</h3>
                <div class="notice">* внешний вид приза может отличаться от изображённого на фотографии</div>
                <div class="bg"
                     style="background-image: url(<?= SITE_TEMPLATE_PATH ?>/images/prize-toolkit-bg.jpg)"></div>
            </div>
        </div>
    </div>
</section>

<section class="section section-participate">
    <div class="participate">
        <h2>Как принять
            участие?</h2>
        <div class="steps">
            <article class="step">
                <h3>
                    <strong>1.</strong>
                    Сними видео
                </h3>
                <p>Сними видео, в котором раскрывается твой профессионализм, и расскажи, почему мы должны выбрать для
                    съемок именно тебя.</p>
            </article>
            <article class="step">
                <h3>
                    <strong>2.</strong>
                    Загрузи видео
                </h3>
                <p><a href="#">Добавь</a> свое видео на этот сайт, раскажи о нем друзьям, чтобы они смогли проголосовать
                    за него.</p>
            </article>
            <article class="step">
                <h3>
                    <strong>3.</strong>
                    Выиграй приз
                </h3>
                <p>Жюри конкурса <strong>каждый месяц</strong> из самых популярных видео выбирает лучшего мастера,
                    которому достанется приз.</p>
            </article>
        </div>
        <div class="actions">
            <div class="inner">
                <div class="item">
                    <button class="btn btn-large" onclick="openSendDialog()">Добавить свое видео</button>
                </div>
                <div class="item">
                    <div class="rules-link">
                        <a href="#"><i class="icon"></i> Правила участия</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
