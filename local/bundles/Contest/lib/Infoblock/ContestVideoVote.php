<?php

namespace App\Bundle\Contest\Infoblock;


use TAO\Infoblock;

class ContestVideoVote extends Infoblock
{
    public function title()
    {
        return 'Голос за видео для конкурса';
    }


    public function messages()
    {
        return array(
            'ELEMENT_NAME' => 'Голос за видео',
            'ELEMENTS_NAME' => 'Голоса за видео',
            'ELEMENT_ADD' => 'Добавить голос за видео',
            'ELEMENT_EDIT' => 'Изменить голос за видео',
            'ELEMENT_DELETE' => 'Удалить голос за видео',
        );
    }

    public function properties()
    {
        return array(
            'VIDEO' => [
                'NAME' => 'Видео',
                'PROPERTY_TYPE' => 'E',
                'LINK_IBLOCK_ID' => \TAO::infoblock('contest_videos')->id()
            ]
        );
    }
}