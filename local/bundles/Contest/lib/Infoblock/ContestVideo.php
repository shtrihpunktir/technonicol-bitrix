<?php

namespace App\Bundle\Contest\Infoblock;


use TAO\Infoblock;

class ContestVideo extends Infoblock
{
    protected $entityClassName = 'App\Bundle\Contest\Entity\ContestVideoEntity';
    public function title()
    {
        return 'Видео для конкурса';
    }

    public function data()
    {
        return array(
            'LIST_PAGE_URL' => '/master/contest',
            'DETAIL_PAGE_URL' => '/master/contest/#ID#/',
        );
    }

    public function messages()
    {
        return array(
            'ELEMENT_NAME' => 'Видео',
            'ELEMENTS_NAME' => 'Видео',
            'ELEMENT_ADD' => 'Добавить видео',
            'ELEMENT_EDIT' => 'Изменить видео',
            'ELEMENT_DELETE' => 'Удалить видео',
        );
    }

    public function properties(){
        return array(
            'EMAIL' => array(
                'NAME' => 'Email',
                'PROPERTY_TYPE' => 'S',
                'IS_REQUIRED' => 'Y'
            ),
            'PHONE' => array(
                'NAME' => 'Телефон',
                'PROPERTY_TYPE' => 'S',
                'IS_REQUIRED' => 'Y'
            ),
            'VIDEO' => array(
                'NAME' => 'Ссылка на видео',
                'PROPERTY_TYPE' => 'S',
                'IS_REQUIRED' => 'Y',
                'HINT' => 'https://api.vk.com/method/video.get?v=5.95&videos={VIDEO_ID}&access_token={TOKEN}<br>\nVIDEO_ID - это цифры вида 2275585_161867259<br>\nTOKEN обновить можно тут https://oauth.vk.com/authorize?client_id=6664591&response_type=token&scope=video'
            ),
            'VIDEO_ID' => array(
                'NAME' => 'ID видео',
                'PROPERTY_TYPE' => 'S',
                'IS_REQUIRED' => 'Y',
            ),
            'EMBED_VIDEO' => array(
                'NAME' => 'Ссылка для проигрывания',
                'PROPERTY_TYPE' => 'S',
                'IS_REQUIRED' => 'N'
            ),
        );
    }
}