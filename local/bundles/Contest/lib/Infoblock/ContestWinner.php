<?php

namespace App\Bundle\Contest\Infoblock;


use TAO\Infoblock;

class ContestWinner extends Infoblock
{
    public function title()
    {
        return 'Победители конкурса';
    }

    public function data()
    {
        return array(
            'LIST_PAGE_URL' => '',
            'DETAIL_PAGE_URL' => '',
        );
    }

    public function messages()
    {
        return array(
            'ELEMENT_NAME' => 'Победитель',
            'ELEMENTS_NAME' => 'Победители конкурса',
            'ELEMENT_ADD' => 'Добавить победителя',
            'ELEMENT_EDIT' => 'Изменить победителя',
            'ELEMENT_DELETE' => 'Удалить победителя',
        );
    }

    public function properties(){
        return array(
            'VIDEO' => array(
                'NAME' => 'Ссылка на видео',
                'PROPERTY_TYPE' => 'S',
                'IS_REQUIRED' => 'Y',
            ),
        );
    }
}