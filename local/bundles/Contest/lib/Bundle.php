<?php

namespace App\Bundle\Contest;


class Bundle extends \TAO\Bundle
{
    const CONTEST_RATING_ID = 'MASTER_VIDEO_CONTEST_RATING';

    public function init()
    {
        $this->infoblockSchema('contest', 'contest_videos', 'ContestVideo');
        $this->infoblockSchema('contest', 'contest_video_votes', 'ContestVideoVote');
        $this->infoblockSchema('contest', 'contest_winners', 'ContestWinner');
    }

    public function cachedInit()
    {
        $this->infoblockType('contest', [
            'NAME_ru' => 'Конкурс',
            'NAME_en' => 'Contest',
            'SECTIONS' => 'N'
        ]);
    }

    public function routes()
    {
        return array(
            '{^/master/contest/$}' => array(
                'elements_of' => 'contest_videos',
                'page_mode' => 'selector',
                'page_class' => 'news-selector',
                'per_page' => 4,
                'order' => ['ACTIVE_FROM' => 'desc', 'DATE_CREATE' => 'desc'],
                'page' => $_GET['page'] ?: 1,
                'pager_bottom' => true,
                'list_mode' => 'ajax',
            ),
            '{^/master/contest/(\d+)/vote$}' => array('action' => 'vote', 'id' => '{1}'),
            '{^/master/contest/winners/(\d+)/$}' => array('action' => 'winner', 'id' => '{1}'),
            '{^/master/contest/video$}' => array('action' => 'video'),
        );
    }
}
