<?php

namespace App\Bundle\Contest\Controller;

class Index extends \TAO\Controller
{
    function vote($args)
    {
        global $USER;
        if (!$USER->IsAuthorized()) {
            echo self::jsonResponse(array('error' => 'Auth required'), 401);
            die();
        };
        if (!$this->isPost()) {
            echo self::jsonResponse(array('error' => 'Method not allowed'), 405);
            die();
        };
        $votes = \TAO::infoblock('contest_video_votes');
        $videos = \TAO::infoblock('contest_videos');

        $video = $videos->loadItem($args['id']);
        if ($video->isVoted($USER->GetID())) {

            echo self::jsonResponse(array('data' => $video->render()));
            die();
        }
        $vote = $votes->makeItem();
        $vote['CREATED_BY'] = $USER->GetID();
        $vote['VIDEO'] = $args['id'];
        $vote['NAME'] = $USER->getFullName() . ' за ' . $args['id'];
        $vote->save();


        echo self::jsonResponse(array('element' => $video->render()));
        die();
    }

    public function video()
    {

        $videos = \TAO::infoblock('contest_videos');
        $video = $videos->makeItem();
        $request = $this->getRequest();
        $video['NAME'] = $request->getPost('NAME');
        $video['EMAIL'] = $request->getPost('EMAIL');
        $video['PHONE'] = $request->getPost('PHONE');
        $video['PREVIEW_TEXT'] = $request->getPost('PREVIEW_TEXT');
        $video['DETAIL_TEXT'] = $request->getPost('DETAIL_TEXT');
        $video['ACTIVE'] = 'N';
        if (!$video->setVideo($request->getPost('VIDEO'))) {
            echo self::jsonResponse(['error' => 'invalid video url'], 400);
            die();
        }
        $video->save();
        echo self::jsonResponse($video);
        die();
    }

    public function winner($args){
        $winners = \TAO::infoblock('contest_winners');
        $winner = $winners->loadItem($args['id']);
        print $winner->render(['mode' => 'main']);
        die();
    }

    protected function jsonResponse($response, $status = 200)
    {
        $this->noLayout();
        header('Content-Type: application/json');
        return json_encode(array(
            'success' => $status < 400,
            'status' => $status,
            'data' => $response
        ));
    }

    protected function isPost()
    {
        return strtoupper($this->getRequest()->getRequestMethod()) === 'POST';
    }

    protected function getRequest()
    {
        return \Bitrix\Main\Context::getCurrent()->getRequest();
    }

}