<?php

namespace App\Bundle\Contest\Entity;

class ContestVideoEntity extends \TAO\Entity
{


    function getVotesCount()
    {
        return \TAO::getInfoblock('contest_video_votes')->getCount([
            'filter' => [
                'PROPERTY_VIDEO' => $this->id()
            ]
        ]);
    }

    function isVoted($userId = null)
    {
        if (empty($userId)) {
            global $USER;
            if (!$USER->isAuthorized()) return false;
            $userId = $USER->GetID();
        }
        $votes = \TAO::infoblock('contest_video_votes');
        $count = $votes->getCount(array(
            'filter' => array(
                'PROPERTY_VIDEO' => $this->id(),
                'CREATED_BY' => $userId
            )
        ));
        return $count > 0;

    }

    function setVideo($video)
    {
        $parsed = parse_url($video);
        switch ($parsed['host']) {
            case 'vk.com':
                return $this->setVkVideo($video, $parsed);
            case 'www.youtube.com':
            case 'youtube.com':
                return $this->setYtFullVideo($video, $parsed);
            case 'youtu.be':
                return $this->setYtShortVideo($video, $parsed);
        }
    }

    /**
     * @param $video
     * @param $parsed
     * @param $match
     * @return bool
     */
    protected function setVkVideo($video, $parsed): bool
    {
        $path = $parsed['path'];
        $query = $parsed['query'];
        if (preg_match('/^\/video(\-?\d+_\d+)/', $path, $match)) {
            $this['VIDEO'] = $video;
            $this['VIDEO_ID'] = $match[1];
            return true;
        } else if (preg_match('/z=video(\-?\d+_\d+)/', $query, $match)) {
            $this['VIDEO'] = $video;
            $this['VIDEO_ID'] = $match[1];
            return true;
        }
        return false;
    }

    /**
     * @param $video
     * @param $parsed
     * @return bool
     */
    protected function setYtFullVideo($video, $parsed): bool
    {
        $match = [];
        if ($parsed['path'] != '/watch') {
            return false;
        }
        if (!preg_match('/v\=(\w+)/', $parsed['query'], $match)) {
            return false;
        }
        $id = $match[1];
        $this['VIDEO'] = $video;
        $this['VIDEO_ID'] = $id;
        $this['EMBED_VIDEO'] = 'https://www.youtube.com/embed/' . $id;
        $this->previewPicture("https://img.youtube.com/vi/${id}/default.jpg");
        return true;
    }

    /**
     * @param $video
     * @param $parsed
     * @return bool
     */
    protected function setYtShortVideo($video, $parsed): bool
    {
        $id = str_replace($parsed['path'], '/');
        $this['VIDEO'] = $video;
        $this['VIDEO_ID'] = $id;
        $this['EMBED_VIDEO'] = 'https://www.youtube.com/embed/' . $id;
        $this->previewPicture("https://img.youtube.com/vi/${id}/default.jpg");
        return true;
    }

}
