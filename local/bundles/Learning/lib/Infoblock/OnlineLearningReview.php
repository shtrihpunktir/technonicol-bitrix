<?php

namespace App\Bundle\Learning\Infoblock;


use TAO\Infoblock;

class OnlineLearningReview extends Infoblock
{
    public function title()
    {
        return 'Отзывы о вебинарах';
    }

    public function data()
    {
        return array(
            'LIST_PAGE_URL' => '',
            'DETAIL_PAGE_URL' => '',
        );
    }

    public function messages()
    {
        return array(
            'ELEMENT_NAME' => 'Отзыв о вебинаре',
            'ELEMENTS_NAME' => 'Отзывы о вебинарах',
            'ELEMENT_ADD' => 'Добавить отзыв о вебинаре',
            'ELEMENT_EDIT' => 'Изменить отзыв о вебинаре',
            'ELEMENT_DELETE' => 'Удалить отзыв о вебинаре',
        );
    }

    public function properties()
    {
        return [
            'REFERRED_TO' => [
                'NAME' => 'Привязать к',
                'PROPERTY_TYPE' => 'E',
                'LINK_IBLOCK_ID' => \TAO::infoblock('learning_online')->getData('ID')
            ]
        ];
    }
}