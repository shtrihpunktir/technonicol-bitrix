<?php

namespace App\Bundle\Learning\Infoblock;


use TAO\Infoblock;

class OfflineLearningReview extends Infoblock
{
    public function title()
    {
        return 'Отзывы о курсах';
    }

    public function data()
    {
        return array(
            'LIST_PAGE_URL' => '',
            'DETAIL_PAGE_URL' => '',
        );
    }

    public function messages()
    {
        return array(
            'ELEMENT_NAME' => 'Отзыв о курсе',
            'ELEMENTS_NAME' => 'Отзывы о курсе',
            'ELEMENT_ADD' => 'Добавить отзыв о курсе',
            'ELEMENT_EDIT' => 'Изменить отзыв о курсе',
            'ELEMENT_DELETE' => 'Удалить отзыв о курсе',
        );
    }

    public function properties()
    {
        return [
            'REFERRED_TO' => [
                'NAME' => 'Привязать к',
                'PROPERTY_TYPE' => 'E',
                'LINK_IBLOCK_ID' => \TAO::infoblock('learning_offline')->getData('ID')
            ]
        ];
    }
}