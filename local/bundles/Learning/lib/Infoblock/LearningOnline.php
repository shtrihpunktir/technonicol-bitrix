<?php

namespace App\Bundle\Learning\Infoblock;


use TAO\Infoblock;
class LearningOnline extends Infoblock {
    public function title()
    {
        return 'Онлайн обучение';
    }

    public function data()
    {
        return array(
            'LIST_PAGE_URL' => '/learning/online',
            'DETAIL_PAGE_URL' => '/learning/online/#ID#/',
        );
    }

    public function messages()
    {
        return array(
            'ELEMENT_NAME' => 'Вебинар',
            'ELEMENTS_NAME' => 'Вебинары',
            'ELEMENT_ADD' => 'Добавить вебинар',
            'ELEMENT_EDIT' => 'Изменить вебинар',
            'ELEMENT_DELETE' => 'Удалить вебинар',
        );
    }

    public function properties()
    {
        return array(
            'REGISTRATION_LINK' => [
                'NAME' => 'Ссылка для регистрации',
                'PROPERTY_TYPE' => 'S',
                'IS_REQUIRED' => 'Y',
                'SORT' => 100
            ],
            'DATE_START' => [
                'NAME' => 'Дата/время начала',
                'PROPERTY_TYPE' => 'S',
                'IS_REQUIRED' => 'Y',
                'USER_TYPE' => 'DateTime',
                'SORT' => 200
            ],
            'DATE_END' => [
                'NAME' => 'Дата/время завершения',
                'PROPERTY_TYPE' => 'S',
                'IS_REQUIRED' => 'Y',
                'USER_TYPE' => 'DateTime',
                'SORT' => 300
            ],
            'DURATION' => [
                'NAME' => 'Длительность',
                'PROPERTY_TYPE' => 'S',
                'SORT' => 400
            ],
            'REVIEW_FORM_URL' => [
                'NAME' => 'Ссылка на сбор отзывов',
                'PROPERTY_TYPE' => 'S'
            ],
        );
    }
}