<?php

namespace App\Bundle\Learning\Infoblock;


use TAO\Infoblock;
class LearningOffline extends Infoblock {
    public function title()
    {
        return 'Очное обучение';
    }

    public function data()
    {
        return array(
            'LIST_PAGE_URL' => '/learning/offline',
            'DETAIL_PAGE_URL' => '/learning/offline/#ID#/',
        );
    }

    public function messages()
    {
        return array(
            'ELEMENT_NAME' => 'Курсы',
            'ELEMENTS_NAME' => 'Курс',
            'ELEMENT_ADD' => 'Добавить курс',
            'ELEMENT_EDIT' => 'Изменить курс',
            'ELEMENT_DELETE' => 'Удалить курс',
        );
    }

    public function properties()
    {
        return array(
            'DATE_START' => [
                'NAME' => 'Дата/время начала',
                'PROPERTY_TYPE' => 'S',
                'IS_REQUIRED' => 'Y',
                'USER_TYPE' => 'Date',
                'SORT' => 100
            ],
            'DATE_END' => [
                'NAME' => 'Дата/время завершения',
                'PROPERTY_TYPE' => 'S',
                'IS_REQUIRED' => 'Y',
                'USER_TYPE' => 'Date',
                'SORT' => 200
            ],
            'REVIEW_FORM_URL' => [
                'NAME' => 'Ссылка на сбор отзывов',
                'PROPERTY_TYPE' => 'S'
            ],
        );
    }
}