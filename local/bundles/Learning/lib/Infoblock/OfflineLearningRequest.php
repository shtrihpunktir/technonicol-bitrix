<?php


namespace App\Bundle\Learning\Infoblock;


class OfflineLearningRequest extends \TAO\Infoblock
{
    public function title()
    {
        return 'Заявки на очное обучение';
    }

    public function properties()
    {
        return array(
            'EMAIL' => array(
                'NAME' => 'E-Mail',
                'SORT' => '200',
                'PROPERTY_TYPE' => 'S',
                'IS_REQUIRED' => 'Y',
            ),

            'PHONE' => array(
                'NAME' => 'Телефон',
                'SORT' => '600',
                'PROPERTY_TYPE' => 'S',
                'IS_REQUIRED' => 'Y',
            ),
            'COURSE' => [
                'NAME' => 'Курс',
                'PROPERTY_TYPE' => 'E',
                'LINK_IBLOCK_ID' => \TAO::infoblock('learning_offline')->getData('ID')
            ]
        );

    }


}