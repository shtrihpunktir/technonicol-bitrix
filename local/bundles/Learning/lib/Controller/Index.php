<?php

namespace App\Bundle\Learning\Controller;
class Index extends \TAO\Controller
{
    function showForm()
    {
        $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
        $item = \TAO::infoblock('learning_offline_request')->makeItem();
        foreach (self::fields() as $field) {
            $item[strtoupper($field)] = $request->getPost($field);
        }

        $result = $item->save();
        print_r($item);
        print_r($result);
        exit();
    }

    static function fields()
    {
        return [
            'name',
            'phone',
            'email',
            'course',
        ];
    }
}