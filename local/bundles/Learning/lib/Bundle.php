<?php

namespace App\Bundle\Learning;

class Bundle extends \TAO\Bundle
{
    public function init()
    {
        $this->infoblockSchema('learning', 'learning_online', 'LearningOnline');
        $this->infoblockSchema('learning', 'learning_online_review', 'OnlineLearningReview');
        $this->infoblockSchema('learning', 'learning_offline', 'LearningOffline');
        $this->infoblockSchema('learning', 'learning_offline_review', 'OfflineLearningReview');
        $this->infoblockSchema('learning', 'learning_offline_request', 'OfflineLearningRequest');
//        $this->infoblockSchema('academy', 'academy_banners', 'Banners');
    }

    public function cachedInit()
    {
        $this->infoblockType('learning', [
            'NAME_ru' => 'Обучение',
            'NAME_en' => 'Learning',
            'SECTIONS' => 'N'
        ]);
    }

    public function routes()
    {
        return array(
            '{^/master/ajax/learning/offline/register}' => array('action' => 'showForm'),
        );
    }
}
