<?php


namespace App\Bundle\Learning\Form;


class OfflineLearningRequest extends \TAO\Form
{
    public function options()
    {
        return array(
            'infoblock' => 'learning_offline_request',
            'layout' => 'block',
            'show_labels' => true,
            'show_placeholders' => false,
            'submit_text' => 'Зарегистрироваться',
            'ajax' => true
        );
    }
    public function useScripts()
    {
        parent::useScripts(); // подключаем дефолтные скрипты
//        $this->bundle->useScript('newform.js');
    }

    public function required()
    {
        return array(
            'name' => 'Ведите ваше имя!',
            'email' => 'Введите ваш E-mail!',
        );
    }

}