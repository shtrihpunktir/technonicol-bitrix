# Обучение

Для того, чтобы добавить раздел обучение на сайт нужно сделать следующее:

1. Перенести файлы темы из папки `/local/templates/learning` в соответствующую папку на сайте
2. Перенести Bundle для модуля `techart/bitrix.tao` из папки `/local/bundles/Learning`  в соответствующую папку на сайте
3. В файле `/local/php_interface/init.php` подключить Bundle по аналогии с существующими 
`\TAO::addBundle('Learning');`
4. Поместить содержимое файлов `/master/edu/index.php` и `/master/webinar/index.php` в необходимые папки на сервере (`/master/webinar` и `/master/edu`)
5. В панели управления Bitrix по адресу `/bitrix/admin/site_edit.php?lang=ru&LID=s1` настроить тему для указанных страниц (`/master/webinar` и `/master/edu`) - выбрать тему "Обучение"  

После входа в панели управления в разделе "Контент" добавится раздел "Обучение"

Структура раздела:

* **Заявки на очное обучение** - данные, которые собираются из формы на странице офлайн обучения
* **Отзывы о курсах** - отзывы для офлайных курсов (добавляются админом вручную)  
* **Очное обучение** - список офлайн курсов   
* **Отзывы о вебинарах** - отзывы для онлайн вебинаров (добавляются админом вручную)     
* **Онлайн обучение** - список предстоящих вебинаров     

## Обработка заявок на офлайн обучение
Для обработки новой заявки на офлайн обучение можно использовать механизм событий Bitrix.

После добавления элемента инфоблока вызывается событие [OnAfterIBlockElementAdd](https://dev.1c-bitrix.ru/api_help/iblock/events/onafteriblockelementadd.php)

Внутри обработчика в полученном массиве `$arFields` нужно сравнить код инфоблока с id инфоблока для заявков

У элементов этого инфоблока помимо основных полей есть дополнительные [свойства](https://dev.1c-bitrix.ru/api_help/iblock/classes/ciblockproperty/index.php)
* EMAIL
* PHONE
* COURSE

Пример:

```php
<?php 
// ....

AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("MyClass", "OnAfterIBlockElementAddHandler"));

class MyClass
{
    // создаем обработчик события "OnAfterIBlockElementAdd"
    function OnAfterIBlockElementAddHandler(&$arFields)
    {
        if($arFields["IBLOCK_ID"] != \TAO::infoblock('learning_offline_request')->id()) {
            // ничего не делать
            return;
        }       
        // продожить обработку
    }
}
``` 

# Конкурс

Для того, чтобы добавить раздел конкурс на сайт нужно сделать следующее:

1. Перенести файлы темы из папки `/local/templates/contest` в соответствующую папку на сайте
2. Перенести Bundle для модуля `techart/bitrix.tao` из папки `/local/bundles/Contest`  в соответствующую папку на сайте
3. В файле `/local/php_interface/init.php` подключить Bundle по аналогии с существующими 
`\TAO::addBundle('Contest');`
5. В панели управления Bitrix по адресу `/bitrix/admin/site_edit.php?lang=ru&LID=s1` настроить тему для указанных страниц (`/master/contest`) - выбрать тему "Contest"  

После входа в панели управления в разделе "Контент" добавится раздел "Конкурс"

Структура раздела:

* **Видео для конкурса** - видео, которые присыдают пользователи
* **Голос за видео для конкурса** - голоса, которые оставляют пользователи  
* **Победители конкурса** - список победителей 

## Модерация видео

Для того, чтобы видео попало на сайт в панели управлению нужно выставить галочку "Активность"

Для видео с Youtube ссылка для показа видео и картинка-превью подтягиваются автоматически.

### Модерация для видео Vkontakte
Для видео с Vkontakte модерацию нужно проводить в ручном режиме, так как работа с данным АПИ [требует клиентский ключ доступ](https://vk.com/dev/video.get) .

**Получение доступа**

Токен можно получить по ссылке
`https://oauth.vk.com/authorize?client_id=6664591&response_type=token&scope=video`.

После перехода по этой ссылку браузер перенаправит по адресу вида 
`https://oauth.vk.com/blank.html#access_token={ВАШ ТОКЕН}&expires_in=86400&user_id=2275585`,
откуда нужно будет скопировать токен. Срок жизни - сутки, поэтому делать это при каждом запросе нет необходимости

**Получение данных**

После получения токента нужно перейти по адресу 
`https://api.vk.com/method/video.get?v=5.95&videos={ID}9&access_token={TOKEN}`, 
где вместо `{ID}` нужно подставить знаичние из поля ID видео, а вместо `{TOKEN}` полученный ранее токен.

В ответе на запрос будет содержаться следующая информация
```json
{
  "response": {
    "count": 1,
    "items": [
      {
        "id": 456239170,
        "owner_id": -42011632,
        "title": "Рулонная черепица ТЕХНОНИКОЛЬ",
        "duration": 30,
        "description": "",
        "date": 1562850178,
        "comments": 0,
        "views": 811,
        "width": 1920,
        "height": 1080,
        "photo_130": "https://pp.userapi.com/c850232/v850232627/14eae2/e9OQdOM34Qw.jpg",
        "photo_320": "https://pp.userapi.com/c850232/v850232627/14eae0/Vt_oR8Yd4gY.jpg",
        "photo_800": "https://pp.userapi.com/c850232/v850232627/14eada/20i8Ju9nCRk.jpg",
        "photo_1280": "https://pp.userapi.com/c850232/v850232627/14eadb/YIwKEyVS8nc.jpg",
        "is_favorite": false,
        "first_frame_320": "https://pp.userapi.com/c850020/v850020627/18f674/t3HU720xfrg.jpg",
        "first_frame_160": "https://pp.userapi.com/c850020/v850020627/18f679/BQfCzDn71xg.jpg",
        "first_frame_4096": "https://pp.userapi.com/c850020/v850020627/18f677/QtmCoH0hGNg.jpg",
        "first_frame_130": "https://pp.userapi.com/c850020/v850020627/18f67a/a9C7tKmrd4k.jpg",
        "first_frame_720": "https://pp.userapi.com/c850020/v850020627/18f675/ZRNXTm53E5s.jpg",
        "first_frame_1024": "https://pp.userapi.com/c850020/v850020627/18f676/6jMlAsJt4fk.jpg",
        "first_frame_1280": "https://pp.userapi.com/c850020/v850020627/18f673/NsnHHFjqkzo.jpg",
        "first_frame_800": "https://pp.userapi.com/c850020/v850020627/18f672/rAoQ0LV_5xI.jpg",
        "player": "https://vk.com/video_ext.php?oid=-42011632&id=456239170&hash=43eda1ee4f47851c&__ref=vk.api&api_hash=15629255077d24105243c61baacb_GIZDONJVHA2Q",
        "can_add": 1,
        "can_comment": 1,
        "can_repost": 1,
        "can_like": 1,
        "can_add_to_faves": 1
      }
    ]
  }
}
```

* Значение поля `player` перенести в панель управления в поле `Ссылка для проигрывания`
* Значение поля `photo_320` или `photo_800` перенести в панель управления в поле `Картинка для анонса`
* Отметить галочку Активность
* Сохранить 

## Роутинг
Для подключения страницы Конкурса необходимо настроить обработку адресов  
Для этого в файле .htaccess добавить следующее условие (расположить перед условием отправляющим на `/bitrix/urlrewrite.php`) 
```
RewriteCond %{REQUEST_FILENAME} !/local/vendor/techart/bitrix.tao/urlrewrite.php$
RewriteCond %{REQUEST_URI} !/bitrix/backup/*
RewriteRule	!\.(js|ico|gif|jpg|jpeg|JPG|png|svg|css|chm|doc|xls|zip|rar|pdf|txt|avi|ttf|woff|eot|woff2|xml)$ /local/vendor/techart/bitrix.tao/urlrewrite.php [L]
```

Для работы AJAX подгрузки списка видео нужно в файл `local/php_interface/init.php` добавить строку 
```
\TAO::setOption('infoblock.contest_videos.elements.ajax', true);
```
